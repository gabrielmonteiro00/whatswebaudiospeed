var speed = 1;

const interval = setInterval(()=>{
    var cont = 0;
    const selector1 = document.querySelector(".LhZF7");
    const selector2 = document.querySelector(".chGSa");
    var header;
    if (selector1){
        header = selector1
    }else{
        if (selector2){
            header = selector2
        }
    }

    if(header){
        clearInterval(interval)
        
        var array = ["0.8x","1x","1.5x","2x","2.5x"];

        //Create and append select list
        var selectList = document.createElement("select");
        selectList.id = "mySelect";
        header.appendChild(selectList);
        
        //Create and append the options
        for (var i = 0; i < array.length; i++) {
            var option = document.createElement("option");
            option.value = array[i];
            option.text = array[i];
            selectList.appendChild(option);
        }

        const button = document.createElement("button");
        button.innerHTML = "2x";
        button.classList.add("speedTwo");
        

        selectList.addEventListener("change",()=>{
            const desiredSpeed = selectList.value;
            
            switch(desiredSpeed){
                case '0.8x':
                    speed = 0.8;
                    break;
                case '1x':
                    speed = 1;
                    break;
                case '1.5x':
                    speed = 1.5;
                    break;
                case '2x':
                    speed = 2;
                    break;
                case '2.5x':
                    speed = 2.5;
                    break;
                default: speed = 1;
            }
        })

        document.getElementById('mySelect').value='1x';
        
    }else{
        cont++;
        if(cont > 5){
            console.log('Não foi possível carregar a extensão... :(');
        }
    }
}, 1000)

const interval2 = setInterval(()=>{
    const audiosConversa = document.querySelectorAll("audio");
    if(audiosConversa){
       audiosConversa.forEach((audio) => {
           audio.playbackRate = speed;
       })
    }
},1000)